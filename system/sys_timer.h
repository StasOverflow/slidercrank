/*
 * sys_timer.h
 *
 *  Created on: 13   нояб. 2019 г.
 *      Author: V.Striukov
 */

#ifndef SYS_TIMER_H_
#define SYS_TIMER_H_

/* Used alongside seconds time counter */
#define _SEC( x_1secValue )                   ( x_1secValue )
#define _MN(  x_1secValue )                   ( (x_1secValue)*60U )
#define _HR(  x_1secValue )                   ( (x_1secValue)*3600U )

#define sys_timer_seconds_diff_get( timer )   ( sys_timer_seconds_get() - timer )
#define sys_timer_seconds_update( timer )     timer = sys_timer_seconds_get()

#define sys_timer_ms_diff_get( timer )        ( (sys_timer_ms_get() - timer) & 0xFFFF )
#define sys_timer_ms_update( timer )          timer = sys_timer_ms_get()

#define sys_timer_tick_diff_get( timer )      ( (sys_timer_tick_get() - timer) & 0xFFFF )
#define sys_timer_tick_update( timer )        timer = sys_timer_tick_get()


typedef unsigned long timestamp_t;


void                sys_timer_timeproc_add( void (*func)(void), uint8_t period );

uint16_t            sys_timer_sec_get( void );
uint16_t            sys_timer_ms_get( void );
uint16_t            sys_timer_tick_get( void );

void                sys_timer_init( void );


#endif /* SYS_TIMER_H_ */
