
#ifndef SYS_PARAM_H_
#define SYS_PARAM_H_

#include <modbus_register_table.h>


typedef struct
{
    uint16_t    param;
    uint16_t    min_value;
    uint16_t    max_value;
    uint16_t    inc_value;
    uint16_t    default_value;

} setup_param_t;

extern setup_param_t setup_params[];

uint16_t   sys_param_extended_set( uint8_t param, uint16_t value );
uint16_t   sys_param_extended_get( uint8_t param );

void  sys_param_setup_init( void );
void  sys_param_setup_callback( void );
void  sys_param_save( uint16_t param );
void  sys_param_calibration( uint8_t param );
void  sys_param_save_standart( void );
void  sys_param_default_set( void );
uint16_t   sys_param_set( uint16_t param, int16_t value );
uint16_t   sys_param_step_get( uint16_t param );
uint16_t   sys_param_adjust( uint16_t param, int16_t ajust_value );
uint16_t   sys_param_get( uint16_t param );
void  sys_param_inc( uint16_t param );

uint16_t   sys_param_compute( uint16_t value, uint16_t coeff_param );
void  sys_param_proc( void );
void  sys_param_init( void );


#endif /* SYS_PARAM_H_ */


