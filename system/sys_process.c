
#include <stm32f10x.h>

#include "sys_process.h"


static unsigned long sys_barrier_nesting;


void sys_barrier_enter( void )
{
	__disable_irq();

	sys_barrier_nesting++;

	__DSB();
	__ISB();


}


void sys_barrier_exit( void )
{
	if( !--sys_barrier_nesting )
	{
		__enable_irq();
	}
}


__attribute__((naked)) uint32_t  sys_irq_disable( void )
{
	__asm volatile
	(
		"mrs    r0, PRIMASK   \n\t"
		"cpsid  i             \n\t"
		"bx     lr            \n\t"
		"dsb                  \n\t"
		"isb                  \n\t"
		:
		:
		: "r0"
	);
}


__attribute__((naked)) uint32_t  sys_irq_restore( uint32_t mask )
{
	__asm volatile
	(
		"msr    PRIMASK, r0   \n\t"
		"mov    r0, #0        \n\t"
		"bx     lr            \n\t"
		: "=r" (mask)
		:
		: "r0"
	);
}


//
// \param mutex is a pointer to mutex that is to be acquired.
// \return Returns 1 if the mutex is acquired successfully or 0 if it is
//
//__attribute__((naked)) u32  sys_mutex_get( u08 *mutex )
uint32_t  sys_mutex_get( uint8_t *mutex )
{
    uint32_t is_get = 0;

//	sys_barrier_enter();

	sys_barrier(  )
	{
		if( *mutex == 0 )
		{
			*mutex = 1;
			is_get = 1;
		}
	}

//	sys_barrier_exit();

//	__asm volatile
//	(
//			"mov    r3, #0        \n\t"
//
//			"mrs    r1, PRIMASK   \n\t"
//			"cpsid  i             \n\t"
//
//			"ldrb   r2, [r0]      \n\t"
//			"cmp    r2, #0        \n\t"
//			"bne    l_exit        \n\t"
//
//			"mov    r3, #1        \n\t"
//			"strb   r3, [r0]      \n\t"
//
//			"l_exit:              \n\t"
//			"mov    r0, r3        \n\t"
//			"msr    PRIMASK, r1   \n\t"
//			"bx     lr            \n\t"
//			: "=r" (is_get)
//			:
//			: "r0", "r1", "r2", "r3"
//	);

	return is_get;
}


void sys_mutex_put( uint8_t *mutex )
{
	*mutex = 0;
}

