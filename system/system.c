/*
 * system.c
 *
 *  Created on: 13 нояб. 2019 г.
 *      Author: V.Striukov
 */

#include <system.h>



void sys_wdt_enable()
{
    /* Set Watchdog Period to 2 secs*/
    IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
    IWDG_SetPrescaler(IWDG_Prescaler_128);      /* divide 40khz clock by 128 */
    IWDG_SetReload(625);                        /* set reload on 625 ticks   */

    /* Start Watchdog */
    IWDG_Enable();
}


void sys_wdt_reset()
{
    IWDG_ReloadCounter();
}


void sys_powerdown_cpu()
{
    PWR_EnterSTANDBYMode();
}
