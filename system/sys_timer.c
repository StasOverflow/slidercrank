/*
 * sys_timer.c
 *
 *  Created on: 13   нояб. 2019 г.
 *      Author: V.Striukov
 */

#include <system.h>

#include "sys_timer.h"

#ifndef TIMEPROC_COUNT_MAX
#define TIMEPROC_COUNT_MAX                    (  8  )
#endif

#define incSysTick( sysTick )              sysTick++
#define incLowTick( lowTick, cnt, presc )  do { if( ++cnt >= presc ) {cnt = 0; \
                                           lowTick++;}} while(0)

typedef struct timeproc_tag
{
    void (*pfunc) (void);

    uint8_t period;
    uint8_t timer;
} timeproc_t;


static volatile uint16_t        sys_timer_100us_counter16;
static volatile uint16_t        sys_timer_1s0_counter16;

static timeproc_t               timeproc[TIMEPROC_COUNT_MAX];
static volatile uint8_t         timeproc_count;



uint16_t  sys_timer_sec_get( void )
{
    return sys_timer_1s0_counter16;
}

uint16_t  sys_timer_tick_get( void )
{
    return sys_timer_100us_counter16;
}

uint16_t  sys_timer_ms_get( void )
{
    return sys_timer_100us_counter16 / 10;
}


void sys_timer_timeproc_add( void (*func)(void), uint8_t period )
{
    sys_barrier_enter();

    if( timeproc_count >= TIMEPROC_COUNT_MAX )
        while( 1 ) ;

    timeproc[timeproc_count].pfunc = func;
    timeproc[timeproc_count].period = period;
    timeproc[timeproc_count].timer = 0;

    timeproc_count ++;

    sys_barrier_exit();
}


void SysTick_Handler( void )
{
    static uint16_t presc_1s0;

    if( ++presc_1s0 >= 10000 )
    {
        presc_1s0 = 0;
        sys_timer_1s0_counter16++;
    }

    incSysTick(sys_timer_100us_counter16);

    for( uint8_t i = 0; i < timeproc_count; i++ )
    {
        timeproc[i].timer += 1;

        if( timeproc[i].timer >= timeproc[i].period )
        {
            timeproc[i].timer = 0;

            timeproc[i].pfunc();
        }
    }
}


void sys_timer_init( void )
{
    /* Configure SysTick for 1ms interrupts */

    SysTick_Config(SystemCoreClock / 10000);
}
