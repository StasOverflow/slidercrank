
#include <system.h>
#include <drivers/adc.h>
#include <drivers/umodbus_enron.h>
#include <drivers/storage.h>
#include <system/sys_service.h>

#include "sys_param.h"


static  uint16_t  sys_param_std[CHARACTERISTICS_COUNT];
static  uint8_t  param_changed;

setup_param_t setup_params[] =
{
    {
        DEBUG_MODE,
        0,
        1,
        1,
        0,
    },

    {
        FULL_CYCLE_RANGE,
        0,
        65535,
        1,
        0,
    },

    {
        THROTTLE_INVERSED,
        0,
        1,
        1,
        0
    },

    {
        CALIBRATION_MAX_SPIN_TIME,
        5,
        30,
        1,
        15,
    },

    {
        A3967_FORCE_ENABLE,
        0,
        1,
        1,
        0,
    },

    {
        ENGINE_SPINS_INITIAL,
        50,
        500,
        1,
        0
    },

    {
        ENGINE_SPINS_IDLE,
        300,
        1000,
        1,
        0,
    },

    {
        ENGINE_SPINS_WORKING,
        800,
        2500,
        1,
        0,
    },
};

#define SETUP_PARAMS_COUNT          ( sizeof(setup_params) / sizeof(setup_params[0]) )


static uint16_t
sys_param_set_via_protocol( uint8_t param, uint16_t value )
{
    switch( param )
    {
    /*
     * case PARAM:
     *   do_smth();
     *   break;
     */
    }

    return sys_param_set(param, value);
}


static uint8_t
sys_param_multiple_set( uint16_t index, uint16_t len, uint8_t* data )
{
    uint8_t exception = UMODBUS_EXCEPTION_NONE;
    uint16_t temp;

    while( len > 0 )
    {
        temp  = *data++ << 8;
        temp |= *data++;

        sys_param_set_via_protocol(index, temp);

        index++;
        len--;
    }

    return exception;
}


static void
sys_param_force_save( void )
{
    uint16_t param;
    uint16_t value;

    for( uint8_t i = 0; i < SETUP_PARAMS_COUNT; i++ )
    {
        param = setup_params[i].param;
        value = sys_param_std[param];

        storage_write(param, value);
    }
}



static uint16_t
sys_param_load( uint16_t param )
{
    return storage_read(param);
}


void  sys_param_save( uint16_t param )
{
    uint16_t value = sys_param_std[param];

    if( storage_read(param) != value )
    {
        storage_write(param, value);
    }
}


void
sys_param_setup_init( void )
{

}


void sys_param_default_set( void )
{

}

void
sys_param_save_standart( void )
{

}


void
sys_param_setup_callback( void )
{
    sys_param_save_standart();
}


uint16_t
sys_param_set( uint16_t param, int16_t value )
{
    uint16_t accepted_value;
    uint8_t index;

    accepted_value = sys_param_get(param);

    if( value != accepted_value )
    {
        for( index = 0; index < SETUP_PARAMS_COUNT; index++ )
        {
            if( setup_params[index].param == param )
            {
                break;
            }
        }

        if( index < SETUP_PARAMS_COUNT )
        {
            switch( param )
            {
//          case CHARGER_1_SETUP_VOLTAGE_H:
//          case CHARGER_2_SETUP_VOLTAGE_H:
//              setup_params[index].max_value = sys_param_std[param + 1];
//              break;
//
//          case CHARGER_1_SETUP_VOLTAGE_F:
//          case CHARGER_2_SETUP_VOLTAGE_F:
//              setup_params[index].min_value = sys_param_std[param - 1];
//              break;
            }
            if( value < setup_params[index].min_value )
            {
                accepted_value = setup_params[index].min_value;
            }
            else
            if( value > setup_params[index].max_value )
            {
                accepted_value = setup_params[index].max_value;
            }
            else
            {
                accepted_value = value;
            }

            param_changed = 1;
        }
        else
        {
            accepted_value = value;
        }

        sys_param_std[param] = accepted_value;
    }

    return accepted_value;
}


uint16_t
sys_param_step_get( uint16_t param )
{
    uint8_t index;
    uint16_t step = 0;

    for( index = 0; index < SETUP_PARAMS_COUNT; index++ )
    {
        if( setup_params[index].param == param )
        {
            break;
        }
    }

    if( index < SETUP_PARAMS_COUNT )
    {
        step = setup_params[index].inc_value;
    }

    return step;
}


uint16_t
sys_param_adjust( uint16_t param, int16_t ajust_value )
{
    return sys_param_set(param, sys_param_std[param] + ajust_value);
}


uint16_t
sys_param_get( uint16_t param )
{
    uint16_t value;

    value = sys_param_std[param];

    return value;
}


uint16_t
sys_param_compute( uint16_t value, uint16_t coeff_param )
{
    uint32_t value32 = value;
#if 0
    /*
     * TODO
     */
    value32   = value32 * sys_param_std[coeff_param];
    value32  += 1 << (GENERAL_MEASUREMENT_RESOLUTION - 1);
    value32 >>= GENERAL_MEASUREMENT_RESOLUTION;
#endif
    return (uint16_t) value32;
}


void
sys_param_inc( uint16_t param )
{
    sys_param_std[param] += 1;
}


void
sys_param_proc( void )
{
    static uint16_t index;
    static uint16_t timer;
    static uint8_t stage;

    switch( stage )
    {
    case 0:
        if( param_changed )
        {
            stage ++;
        }

        break;

    case 1:
        if( param_changed )
        {
            param_changed = 0;

            sys_timer_ms_update(timer);
        }

        if( sys_timer_ms_diff_get(timer) >= 3000 )
        {
            stage ++;
        }

        break;

    case 2:
        sys_param_save(setup_params[index].param);

        if( ++index >= SETUP_PARAMS_COUNT )
        {
            index = 0;
            stage = 0;

            sys_timer_ms_update(timer);
        }

        break;
    }
}



void
sys_param_init( void )
{
    storage_init(sys_param_force_save);

    umodbus_address_map_register(1000, CHARACTERISTICS_COUNT, sys_param_std, &sys_param_multiple_set);

    for( uint16_t i = 0; i < SETUP_PARAMS_COUNT; i++ )
    {
        sys_param_std[setup_params[i].param] = setup_params[i].default_value;
        sys_param_set(setup_params[i].param, sys_param_load(setup_params[i].param));
    }

#if ( INCLUDE_NET == 1 )
    u08 mac_addr[6];

    mac_addr[0]  = 0x00;
    mac_addr[1]  = 0x12;
    mac_addr[2]  = 0x37;
    mac_addr[3]  = (((sys_params[SYSTEM_SERIAL_NUMBER_0] >>   8) - 0x40) & 0x0F) << 4;
    mac_addr[3] |= (((sys_params[SYSTEM_SERIAL_NUMBER_0] & 0xFF) - 0x40) & 0x0F) << 0;
    mac_addr[4]  = (((sys_params[SYSTEM_SERIAL_NUMBER_1] >>   8) - 0x30) & 0x0F) << 4;
    mac_addr[4] |= (((sys_params[SYSTEM_SERIAL_NUMBER_1] & 0xFF) - 0x30) & 0x0F) << 0;
    mac_addr[5]  = (((sys_params[SYSTEM_SERIAL_NUMBER_2] >>   8) - 0x30) & 0x0F) << 4;
    mac_addr[5] |= (((sys_params[SYSTEM_SERIAL_NUMBER_2] & 0xFF) - 0x30) & 0x0F) << 0;

    sys_params[NET_MAC_ADDR_0] = ((u16)mac_addr[0] << 8) | mac_addr[1];
    sys_params[NET_MAC_ADDR_1] = ((u16)mac_addr[2] << 8) | mac_addr[3];
    sys_params[NET_MAC_ADDR_2] = ((u16)mac_addr[4] << 8) | mac_addr[5];
#endif

    sys_service_add(&sys_param_proc);
}

