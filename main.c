/*
 * main.c
 *
 *  Created on: 12 нояб. 2019 г.
 *      Author: V.Striukov
 */

#include <system.h>
#include <system/sys_timer.h>
#include <system/sys_service.h>
#include <drivers/uart.h>
#include <comm/inter_comm.h>
#include <a3967_stepper.h>

#include <ustdlib.h>



static inline void
gpio_init( void )
{
    GPIO_InitTypeDef gpio;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);


    GPIO_StructInit(&gpio);
    gpio.GPIO_Pin   = GPIO_Pin_13;
    gpio.GPIO_Mode  = GPIO_Mode_Out_PP;
    gpio.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_Init(GPIOC, &gpio);
}


static inline void
hardware_init( void )
{
    gpio_init();
    sys_timer_init();
    sys_wdt_enable();
}


static void
led_blinker( void )
{
    static uint16_t     timer;


    if( sys_timer_ms_diff_get(timer) >= 500 )
    {
        sys_timer_ms_update(timer);
    }
}


void main( void ) __attribute__ ((noreturn));
void main( void )
{
    hardware_init();
    sys_service_add(&led_blinker);

    if( 0 )
    {
        sys_param_init();
        inter_comm_init();
    }
    else
    {
        uart_init(0, 115200, UART_PARITY_EVEN);
    }

    a3967_stepper_init();

    while (1)
    {
        sys_service_run();
        sys_wdt_reset();
    }

}
