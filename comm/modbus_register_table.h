
#ifndef MODBUS_REGISTER_TABLE_H_
#define MODBUS_REGISTER_TABLE_H_


enum Characteristics
{
/**********************************************************
 *
 * Remote Control
 *
 **********************************************************/

    REMOTE_CTRL_CMD,
    REMOTE_CTRL_ARG1,

/**********************************************************
 *
 * Stepper stats (not actually)
 *
 **********************************************************/
    ENGINE_FREQUENCY,           //

    STEPPER_DIRECTION,          //
    STEPPER_AT_LIMIT,           //
    SYSTEM_ACTIVATE,            //
    STEPPER_IN_MOTION,          //
    IS_AUTO_MODE_PIN_STATE,     //

    STEPS_TO_PERFORM,           //
    FULL_CYCLE_RANGE,           //
    STEPS_PERFORMED_LAST_TIME,  //

/**********************************************************
 *
 * System
 *
 **********************************************************/

    SYSTEM_START,               //

    ENGINE_CTRL_STATE = SYSTEM_START,

    // 1 for auto, 0 for manual (can also be remote)
    CONTROL_TYPE,               //
    CTRL_TYPE_AUTO_INPUT_STATE, //
    CTRL_TYPE_MANUAL_INPUT,     //
    CALIBRATION_OK,             //
    SOFTSTART_STATE,            //

    ENGINE_SPINS_INITIAL,       //
    ENGINE_SPINS_IDLE,          //
    ENGINE_SPINS_WORKING,       //

    BATTERY_VOLTAGE,            //

    TEMPERATURE_COOLANT,        //
    TEMPERATURE_OIL,            //
    TEMPERATURE_EXTERNAL,       //

    FUEL_LEVEL,                 //
    OIL_PRESSURE,               //

    THROTTLE_POSITION_CURR,     //
    STARTER_RELAY_STATE,        //
    WARMING_UP_RELAY_STATE,     //
    LOAD_RELAY_STATE,           //
    DUMMY_RELAY_STATE,          //

    EBC_WORK_PERMIT_OUT_STATE,  //
    EBC_OK_STATE,               //
    EBC_IN1_STATE,              //
    EBC_IN2_STATE,              //

    SYSTEM_MODBUS_ADDRESS,      //
    SYSTEM_MODBUS_BAUD,         //
    SYSTEM_MODBUS_PARITY,       //

    REMOTE_CONTROL_ACTIVATE,    //

    SYSTEM_END,                 //

    SYSTEM_LAST = SYSTEM_START + (SYSTEM_END - SYSTEM_START) - 1,

/**********************************************************
 *
 * Configurations
 *
 **********************************************************/
    /* Only available in remote ctrl mode */
    CALIBRATION_MAX_SPIN_TIME,     //
    THROTTLE_POSITION_SETUP,    //
    THROTTLE_INVERSED,          //
    FULL_CYCLE_RANGE_SETUP,     //
    THROTTLE_STOP_MOVING,       //
    THROTTLE_CLOSE,             //
    THROTTLE_OPEN,              //
    param3,
    REMOTELY_ENABLE,            //
    A3967_FORCE_ENABLE,         //

/**********************************************************
 *
 * Raw Data
 *
 **********************************************************/

    ADC_RAW_UAUX,
    ADC_RAW_T_COOLANT,
    ADC_RAW_T_OIL,
    ADC_RAW_T_EXT,
    ADC_RAW_FUEL_LEVEL,
    ADC_RAW_OIL_PRESSURE,

/**********************************************************
 *
 * Debug
 *
 **********************************************************/

    DEBUG_START,

    DEBUG_EXTRA_FIELD_0 = DEBUG_START,
    DEBUG_EXTRA_FIELD_1,
    DEBUG_EXTRA_FIELD_2,
    DEBUG_EXTRA_FIELD_3,

    DEBUG_END,

    DEBUG_LAST = DEBUG_START + (DEBUG_END - DEBUG_START) - 1,

    DEBUG_MODE,

/**********************************************************
 *
 * All Characteristics count
 *
 **********************************************************/

    CHARACTERISTICS_COUNT,

    Parameter_None = CHARACTERISTICS_COUNT,

//*********************************************************
};


#endif /* MODBUS_REGISTER_TABLE_H_ */
