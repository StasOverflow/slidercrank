
#include <system.h>
#include <drivers/umodbus_enron.h>

#include "inter_comm.h"



static uint8_t              module_init;

void
inter_comm_timeout_timer_update( void )
{
}


void
inter_comm_init( void )
{
    if( module_init )
        return;
    else
        module_init = 1;

    umodbus_init(1, 1, 19200, UART_PARITY_EVEN);
}


