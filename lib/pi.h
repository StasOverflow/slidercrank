
#ifndef PI_H_
#define PI_H_

typedef struct
{
	int16_t  kp;     /*  Пропорциональный коэффициент          */
	int16_t  ki;     /*  Интегральный коэффициент              */
	int16_t  u_min;  /*  Минимальное значение воздействия      */
	int16_t  u_max;  /*  Максимальное значение воздействия     */
	int32_t  acc_i;  /*  Текущее значение аккумулятора ошибки  */

} pi_t;

int16_t pi_control( pi_t *pid, int16_t error, uint16_t period );

#endif /* PI_H_ */
