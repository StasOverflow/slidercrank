//*****************************************************************************
//
// File Name	: 'crc.h'
// Title		: 
// Author		: iotrokha
// Created		: 16.01.2013
// Revised		:
// Version		: 
// Target MCU	: 
// Editor Tabs	:
//
// Description  : 
//
//
// License      : Impuls ltd.
//
//
//*****************************************************************************

#ifndef CRC_H_
#define CRC_H_

#include "types.h"

uint16_t    crc_16_ibm( uint8_t * pucFrame, uint16_t usLen );
uint8_t     crc_8_dallas( uint8_t *pcBlock, uint8_t len );

#endif /* CRC_H_ */
