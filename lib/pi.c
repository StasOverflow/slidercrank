
#include <system.h>

#include "pi.h"


#define RESOLUTION                  (  13  )


static
int16_t fmul32( int16_t value, int16_t c )
{
	int32_t temp = value;

	temp  *= c;
	temp >>= RESOLUTION;

	return temp;
}


int16_t pi_control( pi_t *pi, int16_t error, uint16_t period )
{
	int16_t  new_p;   /* новое значение пропорциональной составляющей */
	int16_t  new_i;   /* новое значение интегральной составляющей     */
	int32_t  rem_i;   /* остаток от интегральной составляющей         */
	int16_t  u;       /* выходная величина                            */

	/* Расчет новых значений PID */
	new_p = fmul32(error, pi->kp);
	new_i = fmul32(error + pi->acc_i, pi->ki * period);

	/* Расчет выходной величины */
	u = new_p + new_i;

	/* Проверка границ */
	if( u > pi->u_max )
	{
		u = pi->u_max;

		rem_i = u - new_p;

		if( rem_i > 0 )
		{
			pi->acc_i = ((rem_i << RESOLUTION) / (pi->ki * period)) - error;
		}
		else
		{
			pi->acc_i = 0;
		}
	}
	else
	if( u < pi->u_min )
	{
		u = pi->u_min;

		rem_i = pi->u_min - new_p;

		if( rem_i < 0 )
		{
			pi->acc_i = ((rem_i << RESOLUTION) / (pi->ki * period)) - error;
		}
		else
		{
			pi->acc_i = 0;
		}
	}
	else
	{
		pi->acc_i += error;
	}

	return u;
}
