
#include <system.h>

#include <lib/types.h>
#include <lib/crc.h>

#include "uart.h"


static USART_TypeDef  *pusUart;

#define UART_RX_QUEUE_SIZE          16

static unsigned char    UART_RxQueue[UART_RX_QUEUE_SIZE];
static unsigned short   UART_RxQueueReadIndex;
static unsigned short   UART_RxQueueWriteIndex;
static unsigned char    UART_RxQueueFull;

#define UART_TX_QUEUE_SIZE          16

static unsigned char    UART_TxQueue[UART_TX_QUEUE_SIZE];
static unsigned short   UART_TxQueueReadIndex;
static unsigned short   UART_TxQueueWriteIndex;
static unsigned char    UART_TxQueueFull;

static unsigned char    UART_TransmittionComplete;



static long
UART_RxEnqueue( unsigned char ucData )
{
    long xReturn = false;

    if( !UART_RxQueueFull )
    {
        UART_RxQueue[UART_RxQueueWriteIndex] = ucData;

        UART_RxQueueWriteIndex = (UART_RxQueueWriteIndex + 1) % UART_RX_QUEUE_SIZE;

        if( UART_RxQueueWriteIndex == UART_RxQueueReadIndex )
        {
            UART_RxQueueFull = 1;
        }

        xReturn = true;
    }

    return xReturn;
}


static
long UART_RxDequeue( unsigned char *pucData )
{
    long xReturn = false;

    if( (UART_RxQueueReadIndex != UART_RxQueueWriteIndex) || UART_RxQueueFull )
    {
        *pucData = UART_RxQueue[UART_RxQueueReadIndex];

        UART_RxQueueReadIndex = (UART_RxQueueReadIndex + 1) % UART_RX_QUEUE_SIZE;

        UART_RxQueueFull = 0;

        xReturn = true;
    }

    return xReturn;
}


static
long uart_tx_enqueue( unsigned char ucData )
{
    long xReturn = false;

    if( !UART_TxQueueFull )
    {
        UART_TxQueue[UART_TxQueueWriteIndex] = ucData;

        UART_TxQueueWriteIndex = (UART_TxQueueWriteIndex + 1) % UART_TX_QUEUE_SIZE;

        if( UART_TxQueueWriteIndex == UART_TxQueueReadIndex )
        {
            UART_TxQueueFull = 1;
        }

        xReturn = true;
    }

    return xReturn;
}


static
long UART_TxDequeue( unsigned char *pucData )
{
    long xReturn = false;

    if( (UART_TxQueueReadIndex != UART_TxQueueWriteIndex) || UART_TxQueueFull )
    {
        *pucData = UART_TxQueue[UART_TxQueueReadIndex];

        UART_TxQueueReadIndex = (UART_TxQueueReadIndex + 1) % UART_TX_QUEUE_SIZE;

        UART_TxQueueFull = 0;

        xReturn = true;
    }

    return xReturn;
}


void USART1_IRQHandler( void )
{
    unsigned char   ucData;

    if( USART_GetFlagStatus(pusUart, USART_FLAG_NE |
                                     USART_FLAG_FE |
                                     USART_FLAG_PE |
                                     USART_FLAG_ORE) )
    {
        USART_ReceiveData(pusUart);
    }
    else
        if( USART_GetITStatus(pusUart, USART_IT_RXNE) )
    {
        UART_RxEnqueue(USART_ReceiveData(pusUart));
    }
    else
        if( USART_GetITStatus(pusUart, USART_IT_TXE) )
    {
        if( UART_TxDequeue(&ucData) )
        {
            USART_SendData(pusUart, ucData);
        }
        else
        {
            USART_ITConfig(pusUart, USART_IT_TXE, DISABLE);
            UART_TransmittionComplete = 1;
        }
    }
}


int8_t
uart_get( uint8_t *byte )
{
    int8_t  is_get;

    sys_barrier_enter();
    is_get = UART_RxDequeue(byte);
    sys_barrier_exit();

    return is_get;
}


int8_t
uart_put( uint8_t byte )
{
    int8_t  is_ok = false;

    if( uart_tx_enqueue(byte) != false )
    {
        is_ok = true;
        UART_TransmittionComplete = false;
        USART_ITConfig(pusUart, USART_IT_TXE, ENABLE);
    }

    return is_ok;
}


int8_t
uart_tx_is_complete( void )
{
    return UART_TransmittionComplete;
}


void uart_rx_int_enable( void )
{
    USART_ITConfig(pusUart, USART_IT_RXNE, ENABLE);
}


void uart_rx_int_disable( void )
{
    USART_ITConfig(pusUart, USART_IT_RXNE, DISABLE);
}


void uart_flush( void )
{
    unsigned char   ucChar;

    while( uart_get(&ucChar) );

    USART_ReceiveData(pusUart);
}


void uart_init( long port, long baud, unsigned short parity )
{
    USART_InitTypeDef      USART_InitStructure;
    GPIO_InitTypeDef       GPIO_InitStructure;

    ( void ) port;

    pusUart = USART1;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

    /* Configure USART1 Tx (PA.09) as alternate function push-pull */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    /* Configure USART1 Rx (PA.10) as input floating */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    USART_InitStructure.USART_BaudRate = baud;
    USART_InitStructure.USART_WordLength = parity ? USART_WordLength_9b : USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = parity;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

    USART_Init(pusUart, &USART_InitStructure);
    USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
    USART_ITConfig(USART1, USART_IT_TXE, ENABLE);

    NVIC_EnableIRQ(USART1_IRQn);

    USART_Cmd(pusUart, ENABLE);
}

