/*
 * store.h
 *
 *  Created on: 6 апр. 2018 г.
 *      Author: V.Dudnik
 */

#ifndef DRIVERS_STORAGE_H_
#define DRIVERS_STORAGE_H_


uint32_t  storage_read(uint16_t param);
void storage_write(uint16_t param, uint32_t value);
void storage_init(void (*save_cb)(void));


#endif /* DRIVERS_STORAGE_H_ */
