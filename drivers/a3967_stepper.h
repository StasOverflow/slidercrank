/*
 * a3967_stepper.h
 *
 *  Created on: 3 окт. 2019 г.
 *      Author: V.Striukov
 */

#ifndef DRIVERS_A3967_STEPPER_H_
#define DRIVERS_A3967_STEPPER_H_


#define FULL_360_ROTATION_STEPS         400    /* for 1/4 spins per step */


typedef enum {
    Backward,
    Forward,
} direction_t;

typedef enum {
    Turned_Off,
    Idle,
    Turned_On,
    Timeout,
    Rotating,
    Done_Rotating,
} rotation_t;

typedef enum {
    Step_In_Process,
    Step_Done,
} rotation_state_t;

int8_t          a3967_stepper_is_rotating( void );

rotation_t      a3967_stepper_rotation_state_get( void );
int8_t          a3967_stepper_at_limit_state_get( void );

void            a3967_stepper_dir_set( direction_t new_dir );
direction_t     a3967_stepper_dir_get( void );

int32_t         a3967_stepper_step_counter_get( void );
void            a3967_stepper_step_counter_clr( void );

void            a3967_stepper_steps_perform( int32_t step_quantity );
void            a3967_stepper_stop( void );

void            a3967_stepper_step_forever( void );

void            a3967_stepper_init( void );


#endif /* DRIVERS_A3967_STEPPER_H_ */
