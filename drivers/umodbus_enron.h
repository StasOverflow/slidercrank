//*****************************************************************************
//
// File Name    : 'umodbus.h'
// Title        :
// Author       : iotrokha
// Created      : 29.03.2013
// Revised      :
// Version      :
// Target MCU   : AVR ATmega series
// Editor Tabs  :
//
// Description  :
//
//
// License      : Impuls ltd.
//
//
//*****************************************************************************

#ifndef UMODBUS_H_
#define UMODBUS_H_

#include <drivers/uart.h>

#define UMODBUS_INPUT_REGISTER_OFFSET  REG_INPUT_START
#define UMODBUS_INPUT_REGISTER_COUNT   REG_INPUT_NREGS

#define UMODBUS_TRANSMITTER_ENABLE
#define UMODBUS_RECEIVER_ENABLE

enum
{
    UMODBUS_EXCEPTION_NONE                 = 0x00,
    UMODBUS_EXCEPTION_ILLEGAL_FUNCTION     = 0x01,
    UMODBUS_EXCEPTION_ILLEGAL_DATA_ADDRESS = 0x02,
    UMODBUS_EXCEPTION_ILLEGAL_DATA_VALUE   = 0x03,
    UMODBUS_EXCEPTION_SLAVE_DEVICE_FAILURE = 0x04,
};

typedef uint8_t (*wfunc_t)(uint16_t addr, uint16_t len, uint8_t *data);



uint8_t umodbus_addr_get( void );
void    umodbus_addr_set(uint8_t address);

void    umodbus_address_map_register( uint16_t addr, uint16_t len, uint16_t *p, wfunc_t write_func );

void    umodbus_init( uint8_t address, int32_t port, uint32_t baud_rate, uint16_t parity );



#endif /* UMODBUS_H_ */
