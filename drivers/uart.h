
#ifndef UART_H_
#define UART_H_


#define UART_PARITY_NONE                         ((uint16_t)0x0000)
#define UART_PARITY_EVEN                         ((uint16_t)0x0400)
#define UART_PARITY_ODD                          ((uint16_t)0x0600)


int8_t  uart_get( uint8_t *byte );
int8_t  uart_put( uint8_t byte );
int8_t  uart_tx_is_complete( void );

void  uart_rx_int_enable( void );
void  uart_rx_int_disable( void );
void  uart_flush( void );

void  uart_init( long port, long baud, unsigned short parity );



#endif /* UART_H_ */
