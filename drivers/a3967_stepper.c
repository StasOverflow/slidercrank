/*
 * a3967_stepper.c
 *
 *  Created on: 3 окт. 2019 г.
 *      Author: V.Striukov
 */
#include <system.h>

#include <uart.h>
#include "a3967_stepper.h"

/**
 *  To find out alternate function mapping for timers, please
 *  refer to
 *  9.3.7 Timer alternate function remapping
 *  (can be found here: https://bit.ly/2OkwTH4 page 178 )
 */
static int8_t           a3967_module_init = false;

static const uint8_t    sin_table[] = {

#if 0
        0,0,0,0,
        0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,
        1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,
        2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,
        3,4,4,4,4,4,4,4,5,5,5,5,5,5,5,6,
        6,6,6,6,7,7,7,7,7,7,8,8,8,8,8,9,
        9,9,9,10,10,10,10,10,11,11,11,11,12,12,12,12,
        13,13,13,13,14,14,14,14,15,15,15,15,16,16,16,17,
        17,17,17,18,18,18,19,19,19,19,20,20,20,21,21,21,
        22,22,22,23,23,23,24,24,24,25,25,25,26,26,26,27,
        27,28,28,28,29,29,29,30,30,31,31,31,32,32,32,33,
        33,34,34,34,35,35,36,36,36,37,37,38,38,39,39,39,
        40,40,41,41,42,42,42,43,43,44,44,45,45,46,46,47,
        47,47,48,48,49,49,50,50,51,51,52,52,53,53,54,54,
        55,55,56,56,57,57,58,58,59,59,60,60,61,61,62,62,
        63,63,64,65,65,66,66,67,67,68,68,69,69,70,70,71,
        72,72,73,73,74,74,75,76,76,77,77,78,78,79,80,80,
        81,81,82,83,83,84,84,85,86,86,87,87,88,89,89,90,
        90,91,92,92,93,93,94,95,95,96,97,97,98,98,99,100,
        100,101,102,102,103,104,104,105,106,106,107,107,108,109,109,110,
        111,111,112,113,113,114,115,115,116,117,117,118,119,120,120,121,
        122,122,123,124,124,125,126,126,127,128,128,129,130,131,131,132,
        133,133,134,135,135,136,137,138,138,139,140,140,141,142,143,143,
        144,145,146,146,147,148,148,149,150,151,151,152,153,154,154,155,
        156,157,157,158,159,160,160,161,162,163,163,164,165,166,166,167,

        168,169,169,170,171,172,172,173,174,175,175,176,177,178,178,179,
        180,181,181,182,183,184,185,185,186,187,188,188,189,190,191,192,
        192,193,194,195,195,196,197,198,199,199,200,201,202,203,203,204,
        205,206,206,207,208,209,210,210,211,212,213,214,214,215,216,217,
        218,218,219,220,221,222,222,223,224,225,226,226,227,228,229,230,
#endif
        230,231,232,233,234,234,235,236,237,238,238,239,240,241,242,242,
        243,244,245,246,246,247,248,249,250,250,251,252,253,254,254,255,
        255,255,
        254,254,253,252,251,250,
        250,249,248,247,246,246,245,244,243,242,242,241,240,239,238,238,
        237,236,235,234,234,233,232,231,230,
#if 0
        230,229,228,227,226,226,225,
        224,223,222,222,221,220,219,218,218,217,216,215,214,214,213,212,
        211,210,210,209,208,207,206,206,205,204,203,203,202,201,200,199,
        199,198,197,196,195,195,194,193,192,192,191,190,189,188,188,187,
        186,185,185,184,183,182,181,181,180,179,178,178,177,176,175,175,
        174,173,172,172,171,170,169,169,168,
        167,166,166,165,164,163,163,
        162,161,160,160,159,158,157,157,156,155,154,154,153,152,151,151,
        150,149,148,148,147,146,146,145,144,143,143,142,141,140,140,139,
        138,138,137,136,135,135,134,133,133,132,131,131,130,129,128,128,
        127,126,126,125,124,124,123,122,122,121,120,120,119,118,117,117,
        116,115,115,114,113,113,112,111,111,110,109,109,108,107,107,106,
        106,105,104,104,103,102,102,101,100,100,99,98,98,97,97,96,
        95,95,94,93,93,92,92,91,90,90,89,89,88,87,87,86,
        86,85,84,84,83,83,82,81,81,80,80,79,78,78,77,77,
        76,76,75,74,74,73,73,72,72,71,70,70,69,69,68,68,
        67,67,66,66,65,65,64,63,63,62,62,61,61,60,60,59,
        59,58,58,57,57,56,56,55,55,54,54,53,53,52,52,51,
        51,50,50,49,49,48,48,47,47,47,46,46,45,45,44,44,
        43,43,42,42,42,41,41,40,40,39,39,39,38,38,37,37,
        36,36,36,35,35,34,34,34,33,33,32,32,32,31,31,31,
        30,30,29,29,29,28,28,28,27,27,26,26,26,25,25,25,
        24,24,24,23,23,23,22,22,22,21,21,21,20,20,20,19,
        19,19,19,18,18,18,17,17,17,17,16,16,16,15,15,15,
        15,14,14,14,14,13,13,13,13,12,12,12,12,11,11,11,
        11,10,10,10,10,10,9,9,9,9,8,8,8,8,8,7,
        7,7,7,7,7,6,6,6,6,6,5,5,5,5,5,5,
        5,4,4,4,4,4,4,4,3,3,3,3,3,3,3,3,
        2,2,2,2,2,2,2,2,2,2,1,1,1,1,1,1,
        1,1,1,1,1,1,1,1,1,
        0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,
        0,0,0,0,
#endif
};

#define SIN_TABLE_SIZE      (sizeof(sin_table)/sizeof(sin_table[0]))

static uint16_t      table[sizeof(sin_table) * 2];

#if 0
#define INITIAL_FREQ_DIVIDER                    4
#define STEPPER_STATE_TRANSITION_TIMEOUT        300
#define LIMIT_SWITCH_FILTER_VALUE               50


static int8_t           a3967_stepper_acceleration = INITIAL_FREQ_DIVIDER;
static int8_t           a3967_stepper_rotating = false;

static direction_t      a3967_stepper_direction = Forward;
static rotation_t       a3967_stepper_state = Idle;
static int32_t          a3967_stepper_steps_to_perform = 0;
static int32_t          a3967_stepper_step_counter = 0;

static uint8_t          a3967_stepper_at_limit = false;
static int8_t           a3967_stepper_enabled_state = false;


int8_t
a3967_stepper_is_rotating( void )
{
    return a3967_stepper_rotating;
}


int8_t
a3967_stepper_at_limit_state_get( void )
{
    return a3967_stepper_at_limit;
}


void
a3967_stepper_dir_set( direction_t new_dir )
{
    a3967_stepper_direction = new_dir;
}


direction_t
a3967_stepper_dir_get( void )
{
    return a3967_stepper_direction;
}


rotation_t
a3967_stepper_rotation_state_get( void )
{
    return a3967_stepper_state;
}


int32_t
a3967_stepper_step_counter_get( void )
{
    return a3967_stepper_step_counter;
}


void
a3967_stepper_step_counter_clr( void )
{
    a3967_stepper_step_counter = 0;
}


void
a3967_stepper_steps_perform( int32_t step_quantity )
{
    a3967_stepper_steps_to_perform = step_quantity;
}

void
a3967_stepper_step_forever( void )
{
    a3967_stepper_steps_to_perform = 0xffff >> 1;   // To fit mbus signed
}


void
a3967_stepper_stop( void )
{
    a3967_stepper_steps_to_perform = 0;   // To fit mbus signed
}


static inline void
a3967_stepper_accelerate( uint32_t steps )
{
    if( steps < 10 )
    {
        a3967_stepper_acceleration = INITIAL_FREQ_DIVIDER;
    }
    else
    if( steps == 500 )
    {
        a3967_stepper_acceleration  -= 1;
    }
    else
    if( steps == 1000 )
    {
        a3967_stepper_acceleration  -= 1;
    }

    if( a3967_stepper_acceleration < 0 )
    {
        a3967_stepper_acceleration = 0;
    }
}


static void
a3967_stepper_timeproc( void )
{
    static uint16_t             rot_state_trans_timeout;
    static uint8_t              times_entered;
    static uint32_t             acceleration_steps;

    static uint8_t              a3967_step_limit_cntr;
    static int8_t               a3967_step_limit_prev;
    int8_t                      a3967_step_limit_curr;
    static enum {
        Pulse_Start,
        Pulse_End,
    } step_pulse_stage;


    a3967_step_limit_curr = LIMIT_SWITCH_REACHED;

    if( a3967_step_limit_curr != a3967_step_limit_prev )
    {
        a3967_step_limit_prev = a3967_step_limit_curr;
        a3967_step_limit_cntr = 0;
    }
    else
    if( ++a3967_step_limit_cntr >= LIMIT_SWITCH_FILTER_VALUE )
    {
        a3967_step_limit_cntr = LIMIT_SWITCH_FILTER_VALUE;
        a3967_stepper_at_limit = a3967_step_limit_curr;
    }

    if( a3967_stepper_direction == Forward )
        STEPPER_DIRECTION_SET_FORWARD;
    else
    if( a3967_stepper_direction == Backward )
        STEPPER_DIRECTION_SET_BACKWARD;

    /* To slow down pulse frequency */
    if( a3967_stepper_state == Rotating )
    {
        if( ++times_entered < a3967_stepper_acceleration )
            return;
        times_entered = 0;
    }
    else
    {
        acceleration_steps = 0;
        times_entered = 0;
    }


    switch( a3967_stepper_state )
    {
    case Turned_Off:
        PIN_CLR(PIN_STEPPER_STEP);
        a3967_stepper_enabled_state = false;

        sys_timer_tick_update(rot_state_trans_timeout);
        a3967_stepper_state = Idle;
        break;

    case Idle:
        PIN_CLR(PIN_STEPPER_STEP);
        step_pulse_stage = Pulse_Start;

        a3967_stepper_rotating = false;

        if( a3967_stepper_steps_to_perform )
        {
            a3967_stepper_state = Turned_On;
        }
        else
        if( sys_timer_tick_diff_get(rot_state_trans_timeout) >= STEPPER_STATE_TRANSITION_TIMEOUT )
        {
             a3967_stepper_state = Turned_Off;
        }
        break;

    case Turned_On:
        sys_timer_tick_update(rot_state_trans_timeout);

        if( a3967_stepper_enabled_state == true )
        {
            a3967_stepper_state = Rotating;
        }
        else
        {
            a3967_stepper_enabled_state = true;
            a3967_stepper_state = Timeout;
        }
        break;

    case Timeout:
        if( sys_timer_tick_diff_get(rot_state_trans_timeout) >= STEPPER_STATE_TRANSITION_TIMEOUT )
        {
            a3967_stepper_state = Rotating;
        }
        break;

    case Rotating:
        a3967_stepper_rotating = true;

        switch( step_pulse_stage )
        {
        case Pulse_Start:
            PIN_SET(PIN_STEPPER_STEP);
            step_pulse_stage = Pulse_End;
            break;

        case Pulse_End:
            PIN_CLR(PIN_STEPPER_STEP);

            a3967_stepper_accelerate(acceleration_steps++);

            a3967_stepper_steps_to_perform--;
            a3967_stepper_step_counter++;

            if( a3967_stepper_steps_to_perform <= 0 )
            {
                a3967_stepper_state = Done_Rotating;
            }

            step_pulse_stage = Pulse_Start;
            break;
        }
        break;

    case Done_Rotating:
        PIN_CLR(PIN_STEPPER_STEP);
        step_pulse_stage = Pulse_Start;
        sys_timer_tick_update(rot_state_trans_timeout);

        a3967_stepper_state = Idle;
        break;

    default:
        a3967_stepper_stop();
        break;
    }


    if( sys_param_get(A3967_FORCE_ENABLE) )
    {
        STEPPER_TURN_ON;
    }
    else
    {
        if( a3967_stepper_enabled_state == true )
        {
            STEPPER_TURN_ON;
        }
        else
        {
            STEPPER_TURN_OFF;
        }
    }
}

#endif



void
a3967_stepper_freq_incr( void )
{
    static uint16_t     timer;
    static uint16_t      pos = 0;
    static uint8_t      dir;
    uint16_t            val = 0;
    uint16_t            newval = 0;
    static char         array[70];

    static int          i;


    if( sys_timer_ms_diff_get(timer) >= 5 )
    {
        sys_timer_ms_update(timer);

        sys_barrier_enter();
        if( pos < SIN_TABLE_SIZE )
        {
            val = (255 - sin_table[pos]) + 255;
//            table[pos*2] = val;
//            TIM4->ARR = val;
//            newval = val/2;
//            TIM4->CCR1 = val-1;
            if( ++pos >= SIN_TABLE_SIZE )
            {
                pos = 0;
            }

//            if( !dir )
//            {
//                pos++;
//            }
//            else
//            {
//                pos--;
//            }
//            if( pos >= 1000 )
//            {
//                dir ^= 1;
//            }
//            if( pos <= 0 )
//            {
//                dir ^= 1;
//            }

            sprintf(array, "%d:%d\r\n", pos, val);

            for( size_t i = 0; i < strlen(array); i++ )
            {
//                uart_put(array[i]);
            }
        }
        sys_barrier_exit();
    }

}


void
a3967_stepper_freq_decr( void )
{
}


static void
a3967_stepper_proc( void )
{
    static uint16_t     timer;
    static uint16_t     incr = 0;

    a3967_stepper_freq_incr();

    if( sys_timer_ms_diff_get(timer) >= 50 )
    {
        sys_timer_ms_update(timer);

        if( incr < SIN_TABLE_SIZE )
        {
//            incr++;
//
//            memcpy(array, 0, sizeof(array));
        }
    }

//    sys_param_set(STEPS_TO_PERFORM, a3967_stepper_steps_to_perform);
//    sys_param_set(STEPS_PERFORMED_LAST_TIME, a3967_stepper_step_counter);
//    sys_param_set(STEPPER_IN_MOTION, a3967_stepper_rotating ? true : false);
}


void
TIM4_IRQHandler( void )
{
    static uint8_t      bit = 0;
    if( TIM4->SR & TIM_EventSource_CC1 )
    {
        bit ^= 1;

        GPIO_WriteBit(GPIOC, GPIO_Pin_13, bit);
    }
}


void
a3967_stepper_init( void )
{

    if( a3967_module_init )
        return;
    else
        a3967_module_init = true;


//    a3967_stepper_state = Turned_Off;
//    a3967_stepper_direction = Forward;
//    a3967_stepper_enabled_state = false;


#define PERIOD 1000
    int TIM_Pulse = 0;
    int i;

    GPIO_InitTypeDef port;
    TIM_TimeBaseInitTypeDef timer;
    TIM_OCInitTypeDef timerPWM;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);

    GPIO_StructInit(&port);
    port.GPIO_Mode = GPIO_Mode_AF_PP;
    port.GPIO_Pin = GPIO_Pin_6;
    port.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOB, &port);

    TIM_TimeBaseStructInit(&timer);
    timer.TIM_Prescaler = 360;
    timer.TIM_Period = 0xff;
    timer.TIM_ClockDivision = 0;
    timer.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit(TIM4, &timer);

    TIM_OCStructInit(&timerPWM);
    timerPWM.TIM_OCMode = TIM_OCMode_PWM1;
    timerPWM.TIM_OutputState = TIM_OutputState_Enable;
    timerPWM.TIM_Pulse = 0x7f;
    timerPWM.TIM_OCPolarity = TIM_OCPolarity_High;
    TIM_OC1Init(TIM4, &timerPWM);       /** Channel 1 Init */

//    TIM4->DIER = (1 << 1);
    NVIC_EnableIRQ(TIM4_IRQn);

    TIM_Cmd(TIM4, ENABLE);

    sys_service_add(&a3967_stepper_proc);
}

