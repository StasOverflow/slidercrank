
#ifndef UMBMASTER_DEFS_H_
#define UMBMASTER_DEFS_H_

#define UMODBUS_ADDRESS_BROADCAST      (   0 )

#define UMODUBS_ADU_SIZE_MIN           (   4 )
#define UMODBUS_ADU_FUNC_SIZE_MIN      ( UMODBUS_ADU_SIZE_MIN + 5 )

#define UMODBUS_ADU_ADDR_OFF           (   0 )


#define UMODBUS_ADU_FUNC_OFF           (   1 )
#define UMODBUS_ADU_REGADDR_OFF        (   2 )
#define UMODBUS_ADU_REGCNT_OFF         (   4 )

#define UMODBUS_FUNC_WRITE_BYTECNT_OFF (   5 )
#define UMODBUS_FUNC_WRITE_VALUE_OFF   (   7 )

#define UMODBUS_FUNC_READ_BYTECNT_OFF  (   2 )
#define UMODBUS_FUNC_READ_DATA_OFF     (   3 )

#define UMODBUS_FUNC_WRITE_SINGLE_OFF  (   4 )


/// Modbus application function code define
#define MBF_READ_DECRETE_INPUTS             0x02
#define MBF_READ_COILS                      0x01
#define MBF_WRITE_SINGLE_COIL               0x05
#define MBF_WRITE_MULTIPLE_COILS            0x0F
#define MBF_READ_INPUT_REGISTERS            0x04
#define MBF_READ_HOLDING_REGISTERS          0x03
#define MBF_WRITE_SINGLE_REGISTER           0x06
#define MBF_WRITE_MULTIPLE_REGISTERS        0x10
#define MBF_READ_WRITE_MULTIPLE_REGISTERS   0x17
#define MBF_MASK_WRITE_REGISTER             0x16
#define MBF_READ_FIFO_QUEUE                 0x18
#define MBF_READ_FILE_RECORD                0x14
#define MBF_WRITE_FILE_RECORD               0x15
#define NBSF_FILE_RECORD                    0x06
#define MBF_READ_EXCEPTION_STATUS           0x07  ///< just for over serial line
#define MBF_DIAGNOSTIC                      0x08  ///< just for over serial line
#define MBF_GET_COMM_EVENT_COUNTER          0x0B  ///< just for over serial line
#define MBF_GET_COMM_EVENT_LOG              0x0C  ///< just for over serial line
#define MBF_REPORT_SLAVE_ID                 0x11  ///< just for over serial line
#define MBF_READ_DEVICE_IDENTIFICATION      0x2B
#define MBF_HAS_EXCEPTION                   0x80
#define MBF_MASK                            0x7F  ///< mask function code

/// following error define
#define MB_IO_PENDING                       3
#define MB_TX_PENDING                       2
#define MB_RX_PENDING                       1
#define MB_ERROR_FREE                       0     ///< it is OK
#define MB_OK                               0     ///< operating is OK
#define MB_ERROR_FORMAT                     -1
#define MB_ERROR_LENGTH                     -2    ///< too short, too long or boudary error
#define MB_ERROR_PARAMETER                  -3    ///< parameter error
#define MB_ERROR_OPEN                       -4    ///< open port error
#define MB_ERROR_TCP_NOT_CONNECTED          -5    ///< TCP port doesn't connect
#define MB_ERROR_NOT_OPENED                 -6    ///< port has not opened
#define MB_ERROR_BUFFER_TOO_SHORT           -7
#define MB_ERROR_NO_FRAME                   -8
#define MB_ERROR_TIMEOUT                    -9
#define MB_ERROR_EXECPTION                  -10
#define MB_ERROR_BAD_FD                     -11
#define MB_ERROR_NET                        -12
#define MB_ERROR_NO_FILE_HANDLE             -13
#define MB_ERROR_PROTOCOL                   -14   ///< Protocol not define
#define MB_ERROR_FUNCTION                   -15
#define MB_ERROR_MODE                       -16


#endif /* UMBMASTER_DEFS_H_ */
