//*****************************************************************************
//
// File Name    : 'umodbus_enron.c'
// Title        :
// Author       : iotrokha
// Created      : 29.03.2013
// Revised      :
// Version      :
// Target MCU   : AVR ATmega series
// Editor Tabs  :
//
// Description  :
//
//
// License      : Impuls ltd.
//
//
//*****************************************************************************


#include <system.h>
#include <lib/crc.h>
#include <comm/inter_comm.h>

#include "umodbus_enron.h"


#define UMODBUS_ADDRESS_BROADCAST      (   0 )

#define UMODBUS_ADU_SIZE_MIN           (   3 )
#define UMODBUS_ADU_FUNC_SIZE_MIN      ( UMODBUS_ADU_SIZE_MIN + 5 )

#define UMODBUS_ADU_ADDR_OFF           (   0 )
#define UMODUBS_ADU_SIZE_MIN           (   4 )

#define UMODBUS_ADU_FUNC_OFF           (   1 )
#define UMODBUS_ADU_REGADDR_OFF        (   2 )
#define UMODBUS_ADU_REGCNT_OFF         (   4 )

#define UMODBUS_FUNC_WRITE_BYTECNT_OFF (   5 )
#define UMODBUS_FUNC_WRITE_VALUE_OFF   (   7 )

#define UMODBUS_FUNC_READ_BYTECNT_OFF  (   2 )
#define UMODBUS_FUNC_READ_DATA_OFF     (   3 )

#define UMODBUS_FUNC_WRITE_SINGLE_OFF  (   4 )

#define UMODBUS_FUNC_ERROR             ( 128 )


#define UMODBUS_FUNC_RW_REGCNT_MAX     ( UMODBUS_BUFFER_LEN / 2 - 5 )
#define UMODBUS_BUFFER_LEN             ( 255 )

#define UMODBUS_ADDR_MAP_MAX_LEN       ( 8 )


enum
{
    UMODBUS_STATE_INIT,
    UMODBUS_STATE_RECEIVE,
    UMODBUS_STATE_FRAME_RECEIVED,
    UMODBUS_STATE_EXECUTE,
    UMODBUS_STATE_TRANSMITTER_INIT,
    UMODBUS_STATE_TRANSMITT,
    UMODBUS_STATE_FRAME_SENT
};

enum
{
    UMODBUS_TIMER_INIT,
    UMODBUS_TIMER_PROCESS,
    UMODBUS_TIMER_STOPPED
};


enum
{
    UMODBUS_MODE_REG_READ,
    UMODBUS_MODE_REG_WRITE
};




struct modbus_addr_map_tag
{
    uint16_t    addr;
    uint16_t    len;
    uint16_t    *read_ptr;
    wfunc_t write_func;
} map_t[UMODBUS_ADDR_MAP_MAX_LEN];



static          uint8_t umodbus_address;
static          uint8_t umodbus_address_new;

static          uint8_t umodbus_state;
static          uint8_t umodbus_status;
static          uint8_t umodbus_timer_state;
static          uint16_t umodbus_t35_timeout;

static          uint8_t umodbus_rxbuf_pos;
static          uint8_t umodbus_txbuf_pos, umodbus_txbuf_count;
static          uint8_t umodbus_buf[UMODBUS_BUFFER_LEN];



void umodbus_addr_set( uint8_t addr )
{
    umodbus_address_new = addr;
}


uint8_t  umodbus_addr_get( void )
{
    return umodbus_address;
}


void
umodbus_address_map_register( uint16_t addr, uint16_t len, uint16_t *p, wfunc_t write_func )
{
    for( uint8_t i = 0; i < UMODBUS_ADDR_MAP_MAX_LEN; i++ )
    {
        if( map_t[i].len == 0 )
        {
            map_t[i].addr = addr;
            map_t[i].len = len;
            map_t[i].read_ptr = p;
            map_t[i].write_func = write_func;
            break;
        }
    }
}


static inline uint8_t
umodbus_timer_proc( void )
{
    static uint16_t umodbus_t35_timer;
    bool       trigger = false;

    switch( umodbus_timer_state )
    {
    case UMODBUS_TIMER_INIT:
        sys_timer_tick_update(umodbus_t35_timer);
        umodbus_timer_state = UMODBUS_TIMER_PROCESS;
        break;
    case UMODBUS_TIMER_PROCESS:
        if( sys_timer_tick_diff_get(umodbus_t35_timer) >= umodbus_t35_timeout )
        {
            umodbus_timer_state = UMODBUS_TIMER_STOPPED;
            trigger = true;
        }
        break;
    case UMODBUS_TIMER_STOPPED:
        break;
    }

    return trigger;
}


static uint8_t
umodbus_readwrite_regs( uint8_t *frame_ptr, uint16_t addr, uint16_t count, uint8_t mode  )
{
    uint16_t index;
    uint8_t exception = UMODBUS_EXCEPTION_ILLEGAL_DATA_ADDRESS;


    for( uint8_t i = 0; i < UMODBUS_ADDR_MAP_MAX_LEN; i++ )
    {
        if( map_t[i].len == 0 )
            break;

        index = addr - map_t[i].addr;

        if( (addr >= map_t[i].addr) &&
            ((addr + count) <= (map_t[i].addr + map_t[i].len)) )
        {
            exception = UMODBUS_EXCEPTION_NONE;

            if( mode == UMODBUS_MODE_REG_WRITE )
            {
                // write
                if( map_t[i].write_func != 0 )
                {
                    exception = map_t[i].write_func(
                            index,
                            count,
                            frame_ptr);
                }
            }
            else
            {
                // read
                while( count > 0 )
                {
                    *frame_ptr++ = (uint8_t)(map_t[i].read_ptr[index] >> 8);
                    *frame_ptr++ = (uint8_t)(map_t[i].read_ptr[index] & 0xff);

                    index++;
                    count--;
                }
            }
        }
    }

    return exception;
}


static inline uint8_t
umodbus_execute( uint8_t * len )
{
    uint8_t exception = UMODBUS_EXCEPTION_NONE;
    uint8_t function_code = umodbus_buf[UMODBUS_ADU_FUNC_OFF];
    uint16_t reg_addr, reg_count;

//  if( *len < UMODBUS_ADU_FUNC_SIZE_MIN )
//      return UMODBUS_EXCEPTION_ILLEGAL_DATA_VALUE;

    if( *len < 3 )
        return UMODBUS_EXCEPTION_ILLEGAL_DATA_VALUE;


    reg_addr   = (uint16_t) (umodbus_buf[UMODBUS_ADU_REGADDR_OFF] << 8);
    reg_addr  |= (uint16_t) (umodbus_buf[UMODBUS_ADU_REGADDR_OFF + 1]);

    reg_count  = (uint16_t) (umodbus_buf[UMODBUS_ADU_REGCNT_OFF] << 8);
    reg_count |= (uint16_t) (umodbus_buf[UMODBUS_ADU_REGCNT_OFF + 1]);


    switch( function_code )
    {
    case  3: // Read Holding Registers
    case  4: // Read Input Register
        if( !((reg_count >= 1) && (reg_count <= UMODBUS_FUNC_RW_REGCNT_MAX)) )
            return UMODBUS_EXCEPTION_ILLEGAL_DATA_VALUE;

        umodbus_buf[UMODBUS_FUNC_READ_BYTECNT_OFF] = (uint8_t) (reg_count * 2);

        *len = 3;

        exception = umodbus_readwrite_regs(
                (uint8_t*)&umodbus_buf[UMODBUS_FUNC_READ_DATA_OFF],
                reg_addr,
                reg_count,
                UMODBUS_MODE_REG_READ);

        if( exception == UMODBUS_EXCEPTION_NONE )
        {
            *len += reg_count * 2;
        }
        break;
    case  6: // Write Single Register
        exception = umodbus_readwrite_regs(
                (uint8_t*)&umodbus_buf[UMODBUS_FUNC_WRITE_SINGLE_OFF],
                reg_addr,
                1,
                UMODBUS_MODE_REG_WRITE);

        *len = 6;
        break;
    case 16: // Write Multiple Registers
        if( !((reg_count >= 1) && (reg_count <= UMODBUS_FUNC_RW_REGCNT_MAX)) )
            return UMODBUS_EXCEPTION_ILLEGAL_DATA_VALUE;

        exception = umodbus_readwrite_regs(
                (uint8_t*)&umodbus_buf[UMODBUS_FUNC_WRITE_VALUE_OFF],
                reg_addr,
                reg_count,
                UMODBUS_MODE_REG_WRITE);

        /* If an error occured convert it into a Modbus exception. */
        if( exception == UMODBUS_EXCEPTION_NONE )
        {
            *len = 6;
        }
        break;
    default:
        exception = UMODBUS_EXCEPTION_ILLEGAL_FUNCTION;
    }

    inter_comm_timeout_timer_update();

    return exception;
}


void
umodbus_proc( void )
{
    uint8_t timer_trigger;
    uint8_t exception;
    uint8_t adu_len;
    uint8_t temp;
    uint16_t crc;

    if( !umodbus_status )
        return;

    timer_trigger = umodbus_timer_proc();

    switch ( umodbus_state )
    {
    case UMODBUS_STATE_INIT:
        umodbus_rxbuf_pos = 0;

        umodbus_address  = umodbus_address_new;

        umodbus_timer_state = UMODBUS_TIMER_INIT;
        umodbus_state = UMODBUS_STATE_RECEIVE;
        break;

    case UMODBUS_STATE_RECEIVE:
        if( timer_trigger )
        {
            umodbus_state = ( umodbus_rxbuf_pos == 0 ) ?
                    UMODBUS_STATE_INIT : UMODBUS_STATE_FRAME_RECEIVED;
        }
        else
        if( uart_get(&temp) )
        {
            if( umodbus_rxbuf_pos < UMODBUS_BUFFER_LEN )
            {
                umodbus_buf[umodbus_rxbuf_pos++] = temp;
            }
            else
            {
                umodbus_buf[UMODBUS_BUFFER_LEN-1] = temp;
            }

            umodbus_timer_state = UMODBUS_TIMER_INIT;
        }
        break;

    case UMODBUS_STATE_FRAME_RECEIVED:
        umodbus_state = UMODBUS_STATE_INIT;

        /* Length and CRC check */
        if( (umodbus_rxbuf_pos >= UMODUBS_ADU_SIZE_MIN) &&
            (crc_16_ibm((uint8_t *) umodbus_buf, (uint16_t)umodbus_rxbuf_pos) == 0) )
        {
            if( (umodbus_buf[UMODBUS_ADU_ADDR_OFF] == umodbus_address) ||
                (umodbus_buf[UMODBUS_ADU_ADDR_OFF] == UMODBUS_ADDRESS_BROADCAST) )
            {
                umodbus_state = UMODBUS_STATE_EXECUTE;
            }
        }
        break;

    case UMODBUS_STATE_EXECUTE:
        umodbus_state = UMODBUS_STATE_INIT;

        adu_len = umodbus_rxbuf_pos;

        exception = umodbus_execute(&adu_len);

        if( umodbus_buf[UMODBUS_ADU_ADDR_OFF] != UMODBUS_ADDRESS_BROADCAST )
        {
            if( exception != UMODBUS_EXCEPTION_NONE )
            {
                /* An exception occured. Build an error frame. */
                umodbus_buf[UMODBUS_ADU_FUNC_OFF]    |= UMODBUS_FUNC_ERROR;
                umodbus_buf[UMODBUS_ADU_FUNC_OFF + 1] = exception;
                adu_len = 3; // addr + func + exception
            }

            crc = crc_16_ibm((uint8_t *) umodbus_buf, adu_len);
            umodbus_buf[adu_len++] = (uint8_t)(crc & 0xff);
            umodbus_buf[adu_len++] = (uint8_t)(crc >> 8);

            umodbus_txbuf_count = adu_len;
            umodbus_txbuf_pos = 0;

            umodbus_timer_state = UMODBUS_TIMER_INIT;
            umodbus_state = UMODBUS_STATE_TRANSMITTER_INIT;
        }
        break;

    case UMODBUS_STATE_TRANSMITTER_INIT:
        if( timer_trigger )
        {
            /* Prepare transmitter to send frame */
            UMODBUS_TRANSMITTER_ENABLE;
            umodbus_state = UMODBUS_STATE_TRANSMITT;
        }
        break;

    case UMODBUS_STATE_TRANSMITT:
        while( uart_put( umodbus_buf[umodbus_txbuf_pos]) )
        {
            if( ++umodbus_txbuf_pos >= umodbus_txbuf_count )
            {
                umodbus_timer_state = UMODBUS_TIMER_INIT;
                umodbus_state = UMODBUS_STATE_FRAME_SENT;
                break;
            }
        }
        break;

    case UMODBUS_STATE_FRAME_SENT:
        if( timer_trigger )
        {
            uart_flush();
            UMODBUS_RECEIVER_ENABLE;
            umodbus_state = UMODBUS_STATE_INIT;
        }
        break;
    }
}


void
umodbus_init( uint8_t address, int32_t port, uint32_t baud_rate, uint16_t parity )
{
    (void) port;
    umodbus_address_new = address;
    umodbus_address = address;

    uart_init(0, baud_rate, parity);

    UMODBUS_RECEIVER_ENABLE;

    umodbus_t35_timeout = ( baud_rate >= 19200 ) ?
            20 : 385000 / baud_rate;

    sys_service_add(&umodbus_proc);

    umodbus_status = Enable;
}
