/*
 * system.h
 *
 *  Created on: 12 нояб. 2019 г.
 *      Author: V.Striukov
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

/*
 * As said in ARMv7_M Architecture reference manual:
 *
 * In ARMv7-M, the single-copy atomic processor accesses are:
 * • All byte accesses.
 * • All halfword accesses to halfword-aligned locations.
 * • All word accesses to word-aligned locations
 *
 * Unlike in Atmel series of MCU`s there is no further need to
 * wrap interface functions* in a sys_barrier;
 *
 * *but only in cases that mentioned in ref. manual. Code seq-
 *  uence that operates with critical data that exceeds word
 *  length (i.e. more than uint32_t) should be still wrapped
 *  with sys_barrier
 */

#include <stm32f10x.h>
#include <stm32f10x_conf.h>
#include <system/sys_timer.h>
#include <system/sys_service.h>
#include <system/sys_process.h>
#include <system/sys_param.h>

#include <ustdlib.h>
#include <misc.h>
#include <stdio.h>
#include <string.h>


#define ARRAY_ELEMENT_COUNT(x)         (sizeof (x) / sizeof (x[0]))

#define PIN_SET(ARGS)                  __SET_PIN(ARGS)
#define PIN_CLR(ARGS)                  __CLR_PIN(ARGS)
#define PIN_TOGGLE(ARGS)               __TOGGLE_PIN(ARGS)
#define PIN_STATE(ARGS)                __PIN_STATE(ARGS)
#define PIN_OUTPUT(ARGS)               __PIN_OUTPUT(ARGS)
#define PIN_INPUT(ARGS)                __PIN_INPUT(ARGS)
#define __SET_PIN(PORT_LETTER, N)      GPIO ## PORT_LETTER->BSRR = GPIO_Pin_ ## N
#define __CLR_PIN(PORT_LETTER, N)      GPIO ## PORT_LETTER->BRR = GPIO_Pin_ ## N
#define __TOGGLE_PIN(PORT_LETTER, N)   GPIO ## PORT_LETTER->ODR ^= GPIO_Pin_ ## N
#define __PIN_STATE(PORT_LETTER, N)    ( GPIO ## PORT_LETTER->IDR & GPIO_Pin_ ## N )
#define __PIN_OUTPUT(PORT_LETTER, N)   do { \
                                       GPIO ## PORT_LETTER->MODER &= ~(GPIO_MODER_MODER0 << (N * 2)); \
                                       GPIO ## PORT_LETTER->MODER |=  (0x01UL << (N * 2)); \
                                       } while(0)
#define __PIN_INPUT(PORT_LETTER, N)    do { \
                                       GPIO ## PORT_LETTER->MODER &= ~(GPIO_MODER_MODER0 << (N * 2)); \
                                       GPIO ## PORT_LETTER->MODER |=  (0x00UL << (N * 2)); \
                                       } while(0)


enum { false, true };


void        sys_wdt_enable( void );
void        sys_wdt_reset( void );
void        sys_powerdown_cpu( void );

#endif /* SYSTEM_H_ */
