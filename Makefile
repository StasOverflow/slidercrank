
CPU     = cortex-m3
PART    = STM32F10X_MD
#F_XTAL  = 72000000UL

DEVICE  = Other
PROJECT = SliderCrank
TARGETS = $(BINDIR)/$(PROJECT).elf \
          $(BINDIR)/$(PROJECT).bin \
          size
          

STM32_LIBRARY_PATH    = $(EMLIB)/stm32/Libraries/STM32F10x_StdPeriph_Lib_V3.5.0/Libraries
STM32_DRIVERS_PATH    = $(STM32_LIBRARY_PATH)/STM32F10x_StdPeriph_Driver
CMSIS_PATH            = $(STM32_LIBRARY_PATH)/CMSIS
CORE_PATH             = $(STM32_LIBRARY_PATH)/CMSIS/CM3/CoreSupport
STM32_PART_PATH       = $(CMSIS_PATH)/CM3/DeviceSupport/ST/STM32F10x


# Include the common make definitions
#
include $(EMLIB)/stm32/makedefs

#
# Common definitions
#
COMMON += -D${PART}
COMMON += -DUSE_STDPERIPH_DRIVER
#ifdef F_XTAL
#COMMON += -DHSE_VALUE=${F_XTAL}
#else
#COMMON += -DINTERNAL_CLOCK_SOURCE
#endif

#
# System include 
#
IPATH   = $(CMSIS_PATH)/Include               \
          $(STM32_PART_PATH)/Include          \
          $(CORE_PATH)/                       \
          $(STM32_PART_PATH)/                 \
          $(STM32_DRIVERS_PATH)/inc           \
          lib                                 \
          comm                                \
          drivers                             \
          drivers/indicator                   \
          drivers/indicator/ind_menus         \
          system                              \
          modules                             \
          proc                                \
          .

#
# System path
#
VPATH   = $(STM32_DRIVERS_PATH)/src           \
          $(STM32_PART_PATH)/                 \
          $(CORE_PATH)/                       \
          lib                                 \
          comm                                \
          drivers                             \
          drivers/indicator                   \
          drivers/indicator/ind_menus         \
          system                              \
          modules                             \
          proc                                \
          .


#
# SPL section
#
#
$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/misc.o
$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/system_stm32f10x.o
$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/stm32f10x_gpio.o
$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/stm32f10x_rcc.o
$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/stm32f10x_iwdg.o
$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/stm32f10x_usart.o
$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/stm32f10x_dma.o
$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/stm32f10x_flash.o
$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/stm32f10x_tim.o
#$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/stm32f0xx_adc.o
#$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/stm32f0xx_gpio.o
#$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/stm32f0xx_exti.o
#$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/stm32f0xx_spi.o
#$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/stm32f0xx_tim.o
#$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/stm32f0xx_flash.o
#$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/stm32f0xx_dma.o
#$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/stm32f0xx_syscfg.o
#$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/stm32f0xx_misc.o
#$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/stm32f0xx_pwr.o

#
# Drivers section
#void
#$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/adc.o
$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/uart.o
#$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/uart_sw.o
#$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/storage.o
#$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/temp_sensor.o
$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/umodbus_enron.o
$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/a3967_stepper.o
#$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/umodbus_master.o

## Indicator drivers section
#$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/ind_713061.o
#$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/ind_menu_driver.o
#$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/ind_display_driver.o
#$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/ind_utils.o
#$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/ind_menu_handler.o


#
# Comm section
#
$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/inter_comm.o

#
# Lib section
#
$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/crc.o
#$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/pi.o
$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/ustdlib.o

#
# Modules section
#
#$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/pdu.o
#$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/psu.o
#$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/output.o

#
# System section
#
$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/sys_param.o
$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/system.o
$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/sys_timer.o
$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/sys_service.o
$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/sys_process.o
$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/storage.o
#$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/process.o
#$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/sys_error.o
#$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/sys_timer.o
#$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/sys_service.o
#$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/system.o
#$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/sys_ui.o
#$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/sys_utils.o

#
# Main section
#
$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/main.o
$(BINDIR)/$(PROJECT).elf: ${OBJDIR}/startup_stm32f10x_md.o
